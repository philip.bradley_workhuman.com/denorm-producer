package org.example.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.workhuman.integration.kafka.common.route.RouteConfigurator;
import org.apache.camel.FluentProducerTemplate;
import org.example.events.AwardDeliveredEvent;
import org.example.route.RecognitionDenormConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

import static com.workhuman.integration.kafka.common.CamelKafkaCommonConstants.HEADER_JMS_TYPE;
import static com.workhuman.integration.kafka.common.CamelKafkaCommonConstants.OUTBOX_KAFKA_CONSUMER_IDS;

@Component
public class EventProducer {

    private static final Logger LOG = LoggerFactory.getLogger(EventProducer.class);

    @Autowired
    protected RouteConfigurator routeConfigurator;

    @Autowired
    private RecognitionDenormConfig config;

    @Autowired
    protected FluentProducerTemplate producerTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @EventListener(ApplicationReadyEvent.class)
    public void produceEvents() throws Exception {
        LOG.info("Application is now ready, will produce events");

        long clientId = 33;
        long awarderId = 3099554;
        long awardTypeId = 0;
        long awardId = 157838662;
        String glbcertId = "Q3BJ-FNKYC7-977WZZ";
        long awardeeId = 3099835;
        long awardeeManagerId = 0;
        long clientProgramId = 0;
        long productId = 0;
        LocalDateTime now = LocalDateTime.now();

        AwardDeliveredEvent event = new AwardDeliveredEvent(clientId, awarderId, awardTypeId, awardId, glbcertId, awardeeId, awardeeManagerId, clientProgramId, productId, now, now);

        int eventCount = 5000;

        for (int i = 0; i < eventCount; i++) {
            sendMessage(event);
        }

        LOG.info("Sent {} messages", eventCount);
    }

    private void sendMessage(Object body) throws JsonProcessingException {
        String header = body.getClass().getSimpleName();
        sendMessage(header, body);
    }

    private void sendMessage(String header, Object body) throws JsonProcessingException {

        String json = objectMapper.writeValueAsString(body);
        String endpoint = routeConfigurator.buildProducerUri(config.getInput());

        producerTemplate.withDefaultEndpoint(endpoint).withHeader(HEADER_JMS_TYPE, header).withHeader(OUTBOX_KAFKA_CONSUMER_IDS, "celebrations-denorm").withBody(json).send();
    }
}
