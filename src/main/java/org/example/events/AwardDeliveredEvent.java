package org.example.events;

import java.time.LocalDateTime;

public record AwardDeliveredEvent(
        long clientId,
        long awarderId,
        long awardTypeId,
        long awardId,
        String glbcertId,
        long awardeeId,
        long awardeeManagerId,
        long clientProgramId,
        long productId,
        LocalDateTime publicDate,
        LocalDateTime created) {
}